function addDays () {
	var allDays = document.getElementById("days").innerHTML;
	
    for (i=1 ; i<=9 ; i++) {
		allDays += `<option value="0${i}">0${i}</option>`
	}

	for (i=10 ; i<=31 ; i++) {
		allDays += `<option value="${i}">${i}</option>`
	}

	return allDays;
}

function addMonths () {
	var allMonths = document.getElementById("months").innerHTML;
	
	for (i=1 ; i<=9 ; i++) {
		allMonths += `<option value="0${i}">0${i}</option>`
	}

	for (i=10 ; i<=12 ; i++) {
		allMonths += `<option value="${i}">${i}</option>`
	}
	return allMonths;
}

function addYears () {
	var allYears = document.getElementById("years").innerHTML;
	for (i=2017 ; i<=2022 ; i++) {
		allYears += `<option value="${i}">${i}</option>`
	}
	return allYears;
}

document.getElementById('days').innerHTML = addDays ();
document.getElementById('months').innerHTML = addMonths ();
document.getElementById('years').innerHTML = addYears ();

const auth = firebase.auth();
var logOutBtn = document.getElementById('logOut');
logOutBtn.addEventListener('click', e => {
	firebase.auth().signOut();
	firebase.auth().onAuthStateChanged(firebaseUser => {
	if(firebaseUser) {
		console.log(firebaseUser);

	}
	else {
		console.log('not logged in');
		window.location.href="index.html";
	}
});
});

